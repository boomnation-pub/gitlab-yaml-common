# [1.32.0](https://gitlab.com/boomnation-pub/gitlab-yaml-common/compare/v1.31.0...v1.32.0) (2024-07-31)


### Features

* 🎸 change deploy image dev ([fbbd327](https://gitlab.com/boomnation-pub/gitlab-yaml-common/commit/fbbd327eb2a1232199241df353ed826a099f60e7))

# [1.31.0](https://gitlab.com/boomnation-pub/gitlab-yaml-common/compare/v1.30.3...v1.31.0) (2024-07-04)


### Features

* 🎸 change prod image registry ([ebd52e5](https://gitlab.com/boomnation-pub/gitlab-yaml-common/commit/ebd52e59f4fa42245bb31ba43c22e989bbd03cbd))

## [1.30.1](https://gitlab.com/boomnation-pub/gitlab-yaml-common/compare/v1.30.0...v1.30.1) (2023-09-18)


### Bug Fixes

* **tf:** fix ref ([12cde7a](https://gitlab.com/boomnation-pub/gitlab-yaml-common/commit/12cde7a0e27062c39434d00a5f674d4626b8b713))

# [1.30.0](https://gitlab.com/boomnation-pub/gitlab-yaml-common/compare/v1.29.1...v1.30.0) (2023-09-18)


### Features

* update ref to TF image ([65a3099](https://gitlab.com/boomnation-pub/gitlab-yaml-common/commit/65a3099b2dbf0df2ccf0f80d04a7539f35eea732))

## [1.29.1](https://gitlab.com/boomnation-pub/gitlab-yaml-common/compare/v1.29.0...v1.29.1) (2023-07-07)


### Bug Fixes

* **package:** fix url ([a15b9c2](https://gitlab.com/boomnation-pub/gitlab-yaml-common/commit/a15b9c22a4d401ebe365b359df1d6784c35e0872))
* **version:** version ([28b3b57](https://gitlab.com/boomnation-pub/gitlab-yaml-common/commit/28b3b5746457f4154f7200ed11c07a270d2b19af))

# [1.29.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.28.0...v1.29.0) (2023-06-08)


### Features

* **npm:** gitlab ([1e44d5d](https://gitlab.com/boomnation/gitlab-yaml-common/commit/1e44d5df66032b5717a4ee45f29aacc7e28a9b43))

# [1.28.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.27.0...v1.28.0) (2023-06-08)


### Features

* **npm:** gitlab ([655b07c](https://gitlab.com/boomnation/gitlab-yaml-common/commit/655b07cffc111ff9f76445e43dc50531f1b658f3))

# [1.27.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.26.0...v1.27.0) (2023-06-08)


### Features

* **yaml:** update ([42c7eb3](https://gitlab.com/boomnation/gitlab-yaml-common/commit/42c7eb35df6e07ed6b2a0fbc299500229ffd1929))

# [1.26.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.25.0...v1.26.0) (2023-06-08)


### Features

* **readme:** updaet ([2d4b018](https://gitlab.com/boomnation/gitlab-yaml-common/commit/2d4b018df1f81100f91f3d83da315efdef4c4d9d))

# [1.25.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.24.0...v1.25.0) (2023-06-08)


### Features

* **npm:** remove registry ([41aac57](https://gitlab.com/boomnation/gitlab-yaml-common/commit/41aac576ee4f9341f906e6bd3a96a1297035f1f9))

# [1.24.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.23.0...v1.24.0) (2023-06-08)


### Features

* **npm:** support beta release ([c128158](https://gitlab.com/boomnation/gitlab-yaml-common/commit/c1281588a48f9ef590eea624de44fa8d63e0cfcb))

# [1.23.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.22.0...v1.23.0) (2023-06-08)


### Features

* **npm:** reg ([3507b7e](https://gitlab.com/boomnation/gitlab-yaml-common/commit/3507b7e5335866ade1aa0fefecbc583740e20479))

# [1.22.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.21.0...v1.22.0) (2023-06-08)


### Features

* **npm:** undo old npmrc ([2bbdbd3](https://gitlab.com/boomnation/gitlab-yaml-common/commit/2bbdbd3ed198acee563edca0ddfaf91a8146b92f))

# [1.21.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.20.0...v1.21.0) (2023-06-07)


### Features

* **npm:** auth ([6480c53](https://gitlab.com/boomnation/gitlab-yaml-common/commit/6480c532d812b7cc5bc5619cc86acfc44030232f))

# [1.20.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.19.0...v1.20.0) (2023-06-07)


### Features

* **conflict:** fix it ([b49d5ca](https://gitlab.com/boomnation/gitlab-yaml-common/commit/b49d5caea7e15c52919f5b0c6146c3c3f6d6a8c7))
* **npm:** back to it ([07b60b5](https://gitlab.com/boomnation/gitlab-yaml-common/commit/07b60b5b78dba47fb60d6d27b3c322aae3f3f16a))

# [1.19.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.18.0...v1.19.0) (2023-06-07)


### Features

* **gitlab:** group level ([ae6f7df](https://gitlab.com/boomnation/gitlab-yaml-common/commit/ae6f7df60986e6caf8974409fd2f171c9c2e2346))

# [1.18.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.17.0...v1.18.0) (2023-06-07)


### Features

* **release:** publish at instance level ([5839c78](https://gitlab.com/boomnation/gitlab-yaml-common/commit/5839c786ea8952ef12b533bdabdfaabf3fffc588))

# [1.17.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.16.0...v1.17.0) (2023-06-07)


### Features

* **deploy:** fix this shit ([f345c56](https://gitlab.com/boomnation/gitlab-yaml-common/commit/f345c56a9ba7f39de8eb87a30dbd927a7407c886))

# [1.16.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.15.0...v1.16.0) (2023-06-07)


### Features

* **deploy:** change npm release to point to 1 project ([091ce54](https://gitlab.com/boomnation/gitlab-yaml-common/commit/091ce543ff133c909aa3605f7b7dea0e98b92a74))

# [1.15.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.14.0...v1.15.0) (2023-06-07)


### Features

* **prettier:** fix spacing ([f46d7e2](https://gitlab.com/boomnation/gitlab-yaml-common/commit/f46d7e2df3f95a71d1093bc19932c9619e4725e1))

# [1.14.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.13.0...v1.14.0) (2023-06-07)


### Features

* **release:** only dev and prod ([2c30c8d](https://gitlab.com/boomnation/gitlab-yaml-common/commit/2c30c8d699ef61b562271e33ed26bb66fe1852da))

# [1.13.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.12.0...v1.13.0) (2023-06-07)


### Features

* **common:** add only dev and prod ([a5fd2f3](https://gitlab.com/boomnation/gitlab-yaml-common/commit/a5fd2f3b7383dbc527547444c1d8d4addc567c98))

# [1.12.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.11.0...v1.12.0) (2023-05-31)


### Features

* move to bn ([067aafb](https://gitlab.com/boomnation/gitlab-yaml-common/commit/067aafb0aa5c9bbe8b0b1b62fa8ceebc2b9aaa59))

# [1.11.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.10.0...v1.11.0) (2023-05-31)


### Features

* move to bn ([ed0a4dc](https://gitlab.com/boomnation/gitlab-yaml-common/commit/ed0a4dc34982fe1eea203f834de6bb7bb8a411a7))

# [1.10.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.9.0...v1.10.0) (2023-05-31)


### Features

* move to bn ([9591aa5](https://gitlab.com/boomnation/gitlab-yaml-common/commit/9591aa58af271ecd64f9f688692373c324df4dfa))

# [1.9.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.8.0...v1.9.0) (2023-05-30)


### Features

* move to bn ([ba7d396](https://gitlab.com/boomnation/gitlab-yaml-common/commit/ba7d396a5c48804d37cebbf9c1c5fa7f14b7d9da))

# [1.8.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.7.0...v1.8.0) (2023-05-30)


### Features

* move to bn ([d6282f5](https://gitlab.com/boomnation/gitlab-yaml-common/commit/d6282f5a18323f343e9523071c608f208a825b1e))

# [1.7.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.6.0...v1.7.0) (2023-05-30)


### Features

* move to bn ([4ca137f](https://gitlab.com/boomnation/gitlab-yaml-common/commit/4ca137fa9268bfbe025e15f97d7bd908ff17a3a6))

# [1.6.0](https://gitlab.com/boomnation/gitlab-yaml-common/compare/v1.5.0...v1.6.0) (2023-05-30)


### Bug Fixes

* double pipeline ([ef1a8fd](https://gitlab.com/boomnation/gitlab-yaml-common/commit/ef1a8fd6df796030008f3046cb5e4f93f5997579))


### Features

* add lib workflow rules ([9440819](https://gitlab.com/boomnation/gitlab-yaml-common/commit/94408197e6d5d5c765509451db9e3482b5219cd6))
* move to bn ([7b1458e](https://gitlab.com/boomnation/gitlab-yaml-common/commit/7b1458eeb1bcd92ba8fd6d40e15f1327824ddaa2))
* trigger release ([cf7673e](https://gitlab.com/boomnation/gitlab-yaml-common/commit/cf7673e743070968f308942d36979febd7dce173))

# [1.5.0](https://gitlab.com/revved/gitlab-yaml-common/compare/v1.4.0...v1.5.0) (2023-05-03)


### Features

* **workflow:** back to anchor ([980ac65](https://gitlab.com/revved/gitlab-yaml-common/commit/980ac6589fe4a1bd1edb154822af5efb49c025ec))

# [1.4.0](https://gitlab.com/revved/gitlab-yaml-common/compare/v1.3.0...v1.4.0) (2023-05-02)


### Features

* **workflow:** remove the anchor ^ ([baf1d29](https://gitlab.com/revved/gitlab-yaml-common/commit/baf1d29df43d37e40224d713d9049653933c99af))

# [1.3.0](https://gitlab.com/revved/gitlab-yaml-common/compare/v1.2.0...v1.3.0) (2023-05-02)


### Features

* **merge:** base ([3595eba](https://gitlab.com/revved/gitlab-yaml-common/commit/3595eba5791659895775c35b5c399335bdee049d))
* **workflow:** add beta matcher ([2aba34e](https://gitlab.com/revved/gitlab-yaml-common/commit/2aba34e649d5be7008b4917a0e38ca43571877c6))
* **workflow:** remove exact beta match ([2efd41c](https://gitlab.com/revved/gitlab-yaml-common/commit/2efd41cab3707916543858fca073dcd1bb85d5fe))

# [1.2.0](https://gitlab.com/revved/gitlab-yaml-common/compare/v1.1.0...v1.2.0) (2023-04-27)


### Features

* **rules:** add golf ([840325e](https://gitlab.com/revved/gitlab-yaml-common/commit/840325e1d81b075d3768934d3686603a0e53e039))

# [1.1.0](https://gitlab.com/revved/gitlab-yaml-common/compare/v1.0.2...v1.1.0) (2023-02-28)


### Features

* new node 18 file ([f987041](https://gitlab.com/revved/gitlab-yaml-common/commit/f9870418cf58af1d95d4cf04cc828dce193905a7))

## [1.0.2](https://gitlab.com/revved/gitlab-yaml-common/compare/v1.0.1...v1.0.2) (2023-02-21)


### Bug Fixes

* **publish:** add note to readme ([ea542ea](https://gitlab.com/revved/gitlab-yaml-common/commit/ea542eaa060f2f73ac0a4bdf48057ca538e655ab))

## [1.0.1](https://gitlab.com/revved/gitlab-yaml-common/compare/v1.0.0...v1.0.1) (2023-02-18)


### Bug Fixes

* make deploy depend on lint ([065042c](https://gitlab.com/revved/gitlab-yaml-common/commit/065042c3154f6fe12cbd0df0ae8b2320b476ab6e))
* make deploy depend on lint ([733e7a7](https://gitlab.com/revved/gitlab-yaml-common/commit/733e7a7934e1c49a7bb861d427f3e4401ced21e1))

# 1.0.0 (2023-02-18)


### Bug Fixes

* added lint job and cleaned up tf apply ([d1bb86a](https://gitlab.com/revved/gitlab-yaml-common/commit/d1bb86ad8d7a7cd946a1dcce58e2d2553c803dbe))
* adjust equal comparison ([e509171](https://gitlab.com/revved/gitlab-yaml-common/commit/e50917199137075825c716793fe29190f9ea93b7))
* allow audit job to fail ([45d70a4](https://gitlab.com/revved/gitlab-yaml-common/commit/45d70a42f386433962031e7e0e480add540793f1))
* always run code quality ([1134ae8](https://gitlab.com/revved/gitlab-yaml-common/commit/1134ae8c6d0c43f642d0c0f4df27ad99992a3e84))
* audit add except_releases to audit jobs ([6c2fe00](https://gitlab.com/revved/gitlab-yaml-common/commit/6c2fe005fd9d6222abd3aacab6915cb705a59aa5))
* builds pass for releases ([0b85cec](https://gitlab.com/revved/gitlab-yaml-common/commit/0b85cecc73ea1ebd457a4c6e1bffc72639e2525b))
* builds pass for releases ([0096d3d](https://gitlab.com/revved/gitlab-yaml-common/commit/0096d3d48e532c587151aa185dd89eef4590f7fa))
* coverage report due to gitlab changes ([9b463d5](https://gitlab.com/revved/gitlab-yaml-common/commit/9b463d501ef7cf71bfa261c78cd8fe65a309b6f2))
* deploy stage should be name release for consistency ([bc6d6a5](https://gitlab.com/revved/gitlab-yaml-common/commit/bc6d6a5d3a085e125d3cb679a8682e91e949b7a7))
* deployment branches were wrong ([e16eadd](https://gitlab.com/revved/gitlab-yaml-common/commit/e16eaddee42a23ba71470ef7cb1b5c98d7996a5a))
* develop not development ([f1f2d31](https://gitlab.com/revved/gitlab-yaml-common/commit/f1f2d318bda6b4ab4f503a32516d697bdb63efbe))
* different name for sls env ([85e45c6](https://gitlab.com/revved/gitlab-yaml-common/commit/85e45c652c0c83f28335bdfa14eaf1b8da81b289))
* do not run code quality for release pushes ([78b7f48](https://gitlab.com/revved/gitlab-yaml-common/commit/78b7f483dfff827795acf687ff7237f5dcc92e5e))
* fix bash error ([85fbf52](https://gitlab.com/revved/gitlab-yaml-common/commit/85fbf5284dac9f6212b433d9fc8c73ff9105f11f))
* fix bash error prod ([ff2cb7e](https://gitlab.com/revved/gitlab-yaml-common/commit/ff2cb7e6fc7c6e7760d9c9f72958ff69b28de22e))
* only run sentry releases from tag branches ([639aab4](https://gitlab.com/revved/gitlab-yaml-common/commit/639aab4ee7ae7f64142de25545663eb2f42dc1ce))
* prod env ([005a9cb](https://gitlab.com/revved/gitlab-yaml-common/commit/005a9cbe52b48e0e028c999da054ba17df016eb7))
* removed anchors in favor of extends ([58f5b09](https://gitlab.com/revved/gitlab-yaml-common/commit/58f5b09380c7937dbee7bb207fa00f2cdcb9deff))
* set proper prod env vars for aws ([9804623](https://gitlab.com/revved/gitlab-yaml-common/commit/980462329a39480164c5834dfde7b9936da95a46))
* set proper prod env vars for aws ([7b42d83](https://gitlab.com/revved/gitlab-yaml-common/commit/7b42d8354d72b9b6dcf2fbfc24dec1d44c05a2d8))
* split deploy dev and deploy prod commands ([c15ca49](https://gitlab.com/revved/gitlab-yaml-common/commit/c15ca4903496d2fb3a38b76ddfd2c04a9d6d4a59))
* trying to fix code q report ([e82f88b](https://gitlab.com/revved/gitlab-yaml-common/commit/e82f88b56ae5ad808f97674d5f2130c7112609de))
* trying to fix prod deployments ([f621713](https://gitlab.com/revved/gitlab-yaml-common/commit/f621713aca12f4491cf0a09e53e5c7736f32fd51))
* use xml for coverage file ([b2ca4d6](https://gitlab.com/revved/gitlab-yaml-common/commit/b2ca4d66a2bb97bac427d7d6203e1692ea741392))
* wip fixing prod releases ([f37044f](https://gitlab.com/revved/gitlab-yaml-common/commit/f37044fb4513eecd0b7f0859b10524123e71b719))
* wip fixing prod releases ([73b9b7f](https://gitlab.com/revved/gitlab-yaml-common/commit/73b9b7f5b8eade42fb4226dd981142467ebbc29a))


### Features

* Add audit and print audit ([55d356e](https://gitlab.com/revved/gitlab-yaml-common/commit/55d356e2a49eceda7d21151f6d802caa15c2c73b))
* add bundle size report job ([65b0a5f](https://gitlab.com/revved/gitlab-yaml-common/commit/65b0a5f2ffb114b47aaed8aed843ef7272db8d83))
* add code quality reporting ([a0695d6](https://gitlab.com/revved/gitlab-yaml-common/commit/a0695d61861b9518538169b7335cb84d44f86c8e))
* add deploy appsync api ([a630768](https://gitlab.com/revved/gitlab-yaml-common/commit/a630768f9d007248b43b14a308b1a3b17168accb))
* add parallel tests extends and coverage ([6f3782b](https://gitlab.com/revved/gitlab-yaml-common/commit/6f3782b417b535bb086d5ea74f2427b3a223ac7f))
* add sentry release and only releases ([20ac4c9](https://gitlab.com/revved/gitlab-yaml-common/commit/20ac4c9ff47953f2f16e6612fb5adec0bb6d7a99))
* added deploy and generic test files ([79e5b32](https://gitlab.com/revved/gitlab-yaml-common/commit/79e5b32f8f733fa3d605ab9928e5f818e2144651))
* added file for react app deployment ([4c9b87f](https://gitlab.com/revved/gitlab-yaml-common/commit/4c9b87f3f1e42fee67f8b850209ac0ac727391cc))
* added install deps script with support for legacy deps ([5bf0c20](https://gitlab.com/revved/gitlab-yaml-common/commit/5bf0c2094b1a13f542e78e23e915dac79f31d33f))
* allow manually triggered pipelines to run ([f579dd2](https://gitlab.com/revved/gitlab-yaml-common/commit/f579dd2afb6190440c27139625ed6d76a1f4d165))
* aws env keys for react deploy ([02596a3](https://gitlab.com/revved/gitlab-yaml-common/commit/02596a30ef5739f2da96380bf3dbec4959660b44))
* before script ([f51ea87](https://gitlab.com/revved/gitlab-yaml-common/commit/f51ea87afa7d22fc445b5da4c88f88bd918be330))
* code quality yaml ([93b31bd](https://gitlab.com/revved/gitlab-yaml-common/commit/93b31bd800a68c12392d4a7fdcac431f34728090))
* common anchors ([ac3b8a4](https://gitlab.com/revved/gitlab-yaml-common/commit/ac3b8a4d39a731652aee4a72e47a6af12949bb56))
* common extends ([7490924](https://gitlab.com/revved/gitlab-yaml-common/commit/7490924fbc410e2062b35ff6db4ee62cca3402d1))
* common extends ([cc3fe9f](https://gitlab.com/revved/gitlab-yaml-common/commit/cc3fe9fde1b0f24e03e988c59e0c34a945adf501))
* common extends ([6969f94](https://gitlab.com/revved/gitlab-yaml-common/commit/6969f949a1a4b0b4e95e87dd603e6f0e0a5e9d0a))
* default workflow rules ([f2878c7](https://gitlab.com/revved/gitlab-yaml-common/commit/f2878c77a43809be667e9d10ac920e1cb4e478a7))
* default workflow rules ([957409f](https://gitlab.com/revved/gitlab-yaml-common/commit/957409f60730db9d5c17c7dd8fa6d3e43246ffea))
* deploy ecs ([099fb4f](https://gitlab.com/revved/gitlab-yaml-common/commit/099fb4f2597e7325911956d003a161714fdd0093))
* deploy serverless ([b7ceb70](https://gitlab.com/revved/gitlab-yaml-common/commit/b7ceb7010adceff4c491d947fa25a934ebb3556b))
* deployments ([c7d1aef](https://gitlab.com/revved/gitlab-yaml-common/commit/c7d1aefac8e17e5c199c48efc03c033bb32dadff))
* deployments ([17640e4](https://gitlab.com/revved/gitlab-yaml-common/commit/17640e4853fa3b1ae245963740e633cd1082627c))
* log bundle stats during bundle report ([74fc760](https://gitlab.com/revved/gitlab-yaml-common/commit/74fc7609629fc26e28ad0a0df618721aa189e6e4))
* make deploy jobs only wait for tests ([a1e405e](https://gitlab.com/revved/gitlab-yaml-common/commit/a1e405ec7eb55b601bcce4e17346961e801beb7c))
* migrations ([9a8b700](https://gitlab.com/revved/gitlab-yaml-common/commit/9a8b700878d56cd3ae0f06ee78f019b70a106d1e))
* migrations ([7ef3a52](https://gitlab.com/revved/gitlab-yaml-common/commit/7ef3a52055d120a114d91e4a9e073d765ecd98a9))
* more shared yaml ([accb3c8](https://gitlab.com/revved/gitlab-yaml-common/commit/accb3c829383bcbf89a1bd9aa68ad9585bcaedc6))
* more shared yaml ([fd595e3](https://gitlab.com/revved/gitlab-yaml-common/commit/fd595e33fe2f0c9948675d14cd67df92a1c0033c))
* more shared yaml ([f5a4992](https://gitlab.com/revved/gitlab-yaml-common/commit/f5a4992b88c1992c0d075d4d95ccd360cbc4ce5d))
* more shared yaml ([626b011](https://gitlab.com/revved/gitlab-yaml-common/commit/626b0116a7c32c72aba439e5820080e3fd35ac6e))
* more shared yaml ([eb4879a](https://gitlab.com/revved/gitlab-yaml-common/commit/eb4879a087070139a39c02a07fd68c8d5112f006))
* more shared yaml ([e1ef37f](https://gitlab.com/revved/gitlab-yaml-common/commit/e1ef37f1f2854c5d1389c02b5f8f32159431e329))
* node image version 14.13.3 ([ce65228](https://gitlab.com/revved/gitlab-yaml-common/commit/ce65228a72fcb0d7f9f645c86acd3856707abf14))
* post deploy ([a681088](https://gitlab.com/revved/gitlab-yaml-common/commit/a681088df54a0f6a2f577c64a168b3bdef1d5acb))
* post deploy ([1dc299a](https://gitlab.com/revved/gitlab-yaml-common/commit/1dc299ade71db16757361822fcca323740e7ba08))
* post deploy ([9880a40](https://gitlab.com/revved/gitlab-yaml-common/commit/9880a40395a0317d59910b790907e9d5df5abc7a))
* remove auth and org variables ([0ef9200](https://gitlab.com/revved/gitlab-yaml-common/commit/0ef9200915ac0e1e7627ad216fa5a1e41128fd21))
* remove link aliases step from bundle size report ([348489b](https://gitlab.com/revved/gitlab-yaml-common/commit/348489b463da107c07eba5673e86b183cf8cde7f))
* remove link aliases step from deploy node no swagger ([995c722](https://gitlab.com/revved/gitlab-yaml-common/commit/995c722b42871e20cbb928945b21a426cf0667f1))
* remove vscode file ([458e615](https://gitlab.com/revved/gitlab-yaml-common/commit/458e615d16eb04e60b94813d927578a2537c4ed9))
* rename appsync file ([5161466](https://gitlab.com/revved/gitlab-yaml-common/commit/5161466ba262786f81802c6b5861f0d85211af96))
* sonar cloud ([fed8fee](https://gitlab.com/revved/gitlab-yaml-common/commit/fed8fee8be94c7f56443b2b0934fcdbf35425427))
* sonar cloud ([7ed6fea](https://gitlab.com/revved/gitlab-yaml-common/commit/7ed6fea90d956efdfb7a5ec44c82bfb79801282a))
* sonar cloud ([f488dcd](https://gitlab.com/revved/gitlab-yaml-common/commit/f488dcd52ad07ea311f873a0416e70e44c355af6))
* sonar cloud ([45ac332](https://gitlab.com/revved/gitlab-yaml-common/commit/45ac33242e6934fc9fb996541ee0842111daf913))
* sonar cloud ([b3e02b1](https://gitlab.com/revved/gitlab-yaml-common/commit/b3e02b197918c5c0c60a048f112b2a41b7fe2c1c))
* sonar cloud ([d7fd4ad](https://gitlab.com/revved/gitlab-yaml-common/commit/d7fd4ad7e43141db09fc6b4a01893e7127705e8f))
* start bundle report right after install deps ([4ff2048](https://gitlab.com/revved/gitlab-yaml-common/commit/4ff20485d97870bca66e22e5c4dba4a8d9e36de8))
